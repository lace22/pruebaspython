from math import pi
import math
##########################################
#Comprobar que el input es un numero
def comprobar(rawinput):

	try:
		val = int(rawinput)
		return True
	except ValueError:
		return False

##########################################
#Quitar decimales
def truncate(number, digits):
    stepper = pow(10.0, digits)
    return math.trunc(stepper * number) / stepper
##########################################

##############################
#Comprobar si un numero es par
def asknumber(numero):
    if numero%2==0:
        return True
    else:
        return False
###############################
